import numpy as np
import pandas as pd

NUM_OF_DICES = 10**7
rolls = np.zeros(NUM_OF_DICES)
for i in range(NUM_OF_DICES):
   rolls[i] = np.random.randint(1,7)



def generate_dicing_sums(rolls, M):
  # return the sums array and the index of the last roll of rolls
  sums = [[0, 0]]
  for (index,), roll in np.ndenumerate(rolls):
    sums[-1][0] += roll
    sums[-1][1] += 1
    if sums[-1][0] >= M:
      sums.append([0, 0])
  if sums[-1][0] < M:
    sums.pop()
  sums = np.array(sums)
  sums[:,0] = sums[:,0] - M
  return sums


sums20 = generate_dicing_sums(rolls, 20)
sums10000 = generate_dicing_sums(rolls, 10000)

result_file = open('result.txt', "w+")
result_file.write('mean of sums - M(20):  ' + str(sums20[:,0].mean() ) + '\n' )
result_file.write('std of sums - M(20):  ' + str(sums20[:,0].std() ) + '\n' )
result_file.write('mean of rolls for M = 20:  ' + str(sums20[:,1].mean() ) + '\n' )
result_file.write('std of rolls for M = 20:  ' + str(sums20[:,1].std() ) + '\n' )
result_file.write('mean of sums - M(10000):  ' + str(sums10000[:,0].mean() ) + '\n' )
result_file.write('std of sums - M(10000):  ' + str(sums10000[:,0].std() ) + '\n' )
result_file.write('mean of rolls for M = 10000:  ' + str(sums10000[:,1].mean() ) + '\n' )
result_file.write('std of rolls for M = 10000:  ' + str(sums10000[:,1].std() ) + '\n' )
result_file.close()