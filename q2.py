import pandas as pd
import numpy as np
from geopy.distance import vincenty
from geopy.geocoders import Nominatim
from itertools import izip

geolocator = Nominatim()

JFK = geolocator.geocode("John F. Kennedy International Airport")
fare_data = pd.read_csv('trip_fare_3.csv', encoding='utf-8', iterator=True, chunksize=1, header=0, skipinitialspace=True, parse_dates=[3])
trip_data = pd.read_csv('trip_data_3.csv', encoding='utf-8', iterator=True, chunksize=1, header=0, skipinitialspace=True, parse_dates=[5, 6])

crd_payment_under_5 = np.array([])
crd_payment_over_50 = np.array([])
fares_per_min = np.array([])
fares_per_mile= np.array([])
speeds = np.array([])
distance_ratios = np.array([])
tips_from_JFK = np.array([])
revenues = np.array([])
drivers = []

def get_distance(trip):
    if 'caled_distance' in trip:
        return trip["caled_distance"]
    else:
        trip["caled_distance"]  = vincenty(
            (trip['pickup_latitude'], trip['pickup_longitude']),
            (trip['dropoff_latitude'], trip['dropoff_longitude'])).miles
        return trip["caled_distance"]

def get_distance_ratio(trip):
    if 'distance_ratio' in trip:
        return trip['distance_ratio']
    else:
        trip['distance_ratio'] = get_distance(trip) * 1.0 / trip['trip_distance']
        return trip['distance_ratio']

def append_narray(array, to_be_appended):
    array = np.resize(array, array.size + 1)
    array[-1]  = to_be_appended

def get_pickup_address(trip):
    address = geolocator.reverse( str(trip['pickup_latitude']) + ', ' + str(trip['pickup_longitude']) ).address
    trip['pickup_address'] = address
    return address

def pickup_at_JFK(trip):
    dis = vincenty((JFK.latitude, JFK.longitude), (trip['pickup_latitude'], trip['pickup_longitude']))
    return dis.miles < 4

def get_speed(trip):
    # miles per hour
    if "speed" in trip:
        return trip["speed"]
    else:
        trip['speed'] = trip['trip_distance'] * 3600.0 / trip['trip_time_in_secs']
        return trip['speed']

def get_fare_per_mile(trip, fare):
    if 'fare_per_mile' in trip:
        return trip["fare_per_mile"]
    else:
        trip['fare_per_mile'] = fare['fare_amount'] * 1.0 / trip['trip_distance']
        return trip['fare_per_mile']

def get_fare_per_min(trip, fare):
    if 'fare_per_min' in trip:
        return trip["fare_per_min"]
    else:
        trip['fare_per_min'] = fare['fare_amount'] * 60.0 / trip['trip_time_in_secs']
        return trip['fare_per_min']

def cal_validation(trip, fare):
    # check car and driver
    valid = trip['medallion'] == fare['medallion']
    valid = valid and trip['hack_license'] == fare['hack_license']
    if not valid:
        print('inconsistent license id')
        return valid

    # check payment add up
    valid = valid and fare["total_amount"] == fare['fare_amount'] + fare['surcharge'] + fare['mta_tax'] + fare['tip_amount'] + fare['tolls_amount']
    if not valid:
        print('inconsistent payment amounts')
        return valid


    # check time consistence
    valid = trip['pickup_datetime'] == fare['pickup_datetime']
    time_diff = trip['dropoff_datetime'] - trip['pickup_datetime'] - pd.to_timedelta(trip['trip_time_in_secs'], unit='s')
    valid = valid and (time_diff > pd.to_timedelta(-5, unit='s') ) and (time_diff <pd.to_timedelta(5, unit='s'))
    if not valid:
        print('inconsistent timestamps')
        return valid

    # check location around NY
    valid = valid and (trip['pickup_latitude'] > 35) and (trip['pickup_latitude'] < 45)
    valid = valid and (trip['dropoff_latitude'] > 35) and (trip['dropoff_latitude'] < 45)
    valid = valid and (trip['pickup_longitude'] > - 80) and (trip['pickup_longitude'] < - 70)
    valid = valid and (trip['dropoff_longitude'] > - 80) and (trip['dropoff_longitude'] < - 70)
    if not valid:
        print('inconsistent location')
        return valid

    # check speed less than 200 mph
    valid = valid and get_speed(trip) < 200
    if not valid:
        print('speed too high')
        return valid

    # check fare per mile according to NY taxi fare info
    valid = valid and get_fare_per_mile(trip, fare) > 2.5
    if not valid:
        print('far per mile too low')
        return valid

    # check fare per minute
    valid = valid and get_fare_per_min(trip, fare)

    # check distance
    valid = valid and get_distance_ratio(trip) > 0 and get_distance_ratio(trip) < 1
    if not valid:
        print('line distance larger than driven distance')

    return valid

i = 0
for fare, trip in izip(fare_data, trip_data):
    fare = fare.iloc[0]
    trip = trip.iloc[0]
    i += 1
    print(str(i))

    if cal_validation(trip, fare):
        if fare['total_amount'] <= 5:
            append_narray( crd_payment_under_5, fare['payment_type'] == 'CRD')
        elif fare['total_amount'] >= 50:
            append_narray( crd_payment_over_50, fare['payment_type'] == 'CRD')

        append_narray( fares_per_min, trip["fare_per_min"])
        append_narray( fares_per_mile, trip["fare_per_mile"])
        append_narray( speeds, trip['speed'])
        append_narray( distance_ratios, trip['distance_ratio'])


        if pickup_at_JFK(trip):
            append_narray( revenues, fare['tip_amount'])

        revenue = fare['fare_amount'] + fare['tip_amount']
        append_narray( revenues, revenue)
        drivers.append(fare['hack_license'])


# fare_data = pd.read_csv('trip_fare_3.csv')

result_file = open('q2_result.txt', "w")

num_of_crds_under_5 = (crd_payment_under_5 == True).sum()
num_of_payments_under_5 = crd_payment_under_5.size
result_file.write( 'fraction of crd under 5: ' +
                    str(num_of_crds_under_5)   + ' / '  +
                    str(num_of_payments_under_5) + ' = ' +
                    str(num_of_crds_under_5 * 1.0 /num_of_payments_under_5) +
                    '\n' )

num_of_crds_over_50 = (crd_payment_over_50 == True).sum()
num_of_payments_over_50 = crd_payment_over_50.size
result_file.write( 'fraction of crd under 50: ' +
                    str(num_of_crds_over_50)   + ' / '  +
                    str(num_of_payments_over_50) + ' = ' +
                    str(num_of_crds_over_50 * 1.0 /num_of_payments_over_50) +
                    '\n' )

mean_fare_per_min = fares_per_min.mean()
result_file.write( 'mean fare per minute driven: ' +
                    str(mean_fare_per_min) + '\n' )

median_fare_per_min = fares_per_mile.median()
result_file.write( 'median fare per mile driven: ' +
                    str(median_fare_per_min) + '\n' )

speed_95_percentile = np.percentile(speeds, 95)
result_file.write( '95 percentile of average driving speed in mph: ' +
                    str(speed_95_percentile) + '\n' )

mean_distance_ratio = distance_ratios.mean()
result_file.write( 'average ratio of line distance / distance driven: ' +
                    str(mean_distance_ratio) + '\n' )

mean_tip = tips_from_JFK.mean()
result_file.write( 'average tip for rides from JFK: ' +
                    str(mean_tip) + '\n' )

revenues_of_drivers = pd.DataFrame({'driver': drivers, 'revenue': revenues})
revenues_of_drivers.groupby('driver').sum()
median_driver_revenue = revenues_of_drivers.median()
result_file.write( 'median March Revenue of a taxi driver: ' +
                    str(median_driver_revenue) + '\n' )

result_file.close()